# All Images Loaded

```CRD.AllImagesLoaded``` acts as an image preloader that works for background images also. This behavior can be used, for example, to display a preloading animation while all images are being loaded.

### Example

Before your closing ```<body>``` tag add:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="functions.js"></script>
<script type="text/javascript" src="utils.js"></script>
<script type="text/javascript" src="all-images-loaded.js"></script>
```

Then hook some actions to the script events:

```javascript
// Initializes the preloader
var preloader = new CRD.AllImagesLoaded();

// Hooks to the preloader
jQuery(preloader).on('crd.allimagesloade.default', function(event, success, error, loader) {
	console.log('All images loaded! ('+error.length+' completed with errors)');
});
```
*A packed and merged source is available to use.*

### Arguments

```CRD.AllImagesLoaded``` accepts two arguments:

Argument | Default | Description
-------- | ------- | -----------
element : ```String``` | ```document.body``` | Container of the images and background images that the script must hook to.
options : ```Object``` | ```{ 'selector' : '*', 'background' : false }``` | Options to overwrite default values for selector and flag for background images preload.

### Settings

Option | Type | Default | Description
------ | ---- | ------- | -----------
selector | ```String``` | ```'*'``` | Default selector to match all elements to be hooked to the loader.
background | ```Boolean``` | ```false``` | Flag to set if the script must hook to background images too.

### Events

Event | Params | Description
----- | ------ | -----------
```crd.allimagesloade.default``` | success : ```Array```, error : ```Array```, object : ```CRD.ScrollSpy``` | Default behavior, triggered when all images habe been loaded (with or with out errors).
```crd.allimagesloade.success``` | success : ```Array```, error : ```Array```, object : ```CRD.ScrollSpy``` | Triggered only if all images have been loaded successfully.
```crd.allimagesloade.error``` | success : ```Array```, error : ```Array```, object : ```CRD.ScrollSpy``` | Triggered when at least one image have not been loaded.

### Methods

Method | Arguents | Returns | Description
------ | -------- | ------- | -----------
constructor | element : ```String``` or ```Object```, options : ```Object``` | A reference to the current object or class: ```CRD.AllImagesLoaded```. | Initializes the instance for the provided element or jQuery object with the provided options.
start | --- | A reference to the current object or class: ```CRD.AllImagesLoaded```. | Start the watching for images and background images load.
reset | --- | A reference to the current object or class: ```CRD.AllImagesLoaded```. | Resets all images and background states (defined by classes).
imageLoaded | event : ```Object``` | --- | Function called when an image or an element with a background image is loaded successfully.
imageError | event : ```Object``` | --- | Function called when an image or an element with a background image fails to load.
evalResults | --- | A reference to the current object or class: ```CRD.AllImagesLoaded```. | Evals all the images to test is all the images already triggered their corresponding events (load complete or error).

### Dependencies

- CRD.Utils
- JQuery [http://www.jquery.com](jQuery)

### License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details